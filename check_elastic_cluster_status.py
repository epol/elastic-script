#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# check_elastic_port.py
# This file is part of epol/elastic-script
#
# Copyright (C) 2018  Enrico Polesel
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
import argparse
import requests

parser = argparse.ArgumentParser()
parser.add_argument("-H", "--hostname", help="hostname", required=True)
parser.add_argument("-P", "--port", help="HTTP API port", type=int, default=9200)
parser.add_argument("-u", "--username", help="API username", type=str, required=False)
parser.add_argument("-p", "--password", help="API password", type=str, required=False)
parser.add_argument("-s", "--ssl", help="Use HTTPS API", action='store_true', required=False)

args = parser.parse_args()

if args.ssl:
    protocol = 'https'
else:
    protocol = 'http'

url = "{protocol}://{hostname}:{port}/_cluster/health".format(protocol=protocol,**(vars(args)))
auth = (args.username,args.password)
try:
    r = requests.get(url=url,auth=auth)
    r.raise_for_status()
except:
    print ("UNKNOWN - error connecting to {hostname}".format(hostname=args.hostname))
    sys.exit(3)

health = r.json()

if health['status'] == 'green':
    returnState = "OK"
    exitCode = 0
elif health['status'] == 'yellow':
    returnState = "WARNING"
    exitCode = 1
elif health['status'] == 'red':
    returnState = "CRITICAL"
    exitCode = 2
else:
    returnState = "UNKNOWN"
    exitCode = 3

print("{returnState} - cluster {cluster_name} status is {cluster_status}| time={time}s active_shards={active_shards}".format(returnState=returnState, cluster_name=health['cluster_name'], cluster_status=health['status'], active_shards=health['active_shards'], time=r.elapsed.total_seconds()))
sys.exit(exitCode)
