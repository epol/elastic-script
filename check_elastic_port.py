#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# check_elastic_port.py
# This file is part of epol/elastic-script
#
# Copyright (C) 2018  Enrico Polesel
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
import argparse
import requests

parser = argparse.ArgumentParser()
parser.add_argument("-H", "--hostname", help="hostname", required=True)
parser.add_argument("-P", "--port", help="HTTP API port", type=int, default=9200)
parser.add_argument("-m", "--method", help="HTTP method to use", type=str, default="HEAD")
parser.add_argument("-u", "--username", help="API username", type=str, required=False)
parser.add_argument("-p", "--password", help="API password", type=str, required=False)
parser.add_argument("-s", "--ssl", help="Use HTTPS API", action='store_true', required=False)

args = parser.parse_args()

if args.ssl:
    protocol = 'https'
else:
    protocol = 'http'

url = "{protocol}://{hostname}:{port}/".format(protocol=protocol,**(vars(args)))
auth = (args.username,args.password)
try:
    r = requests.request(method=args.method,url=url,auth=auth)
except:
    print ("UNKNOWN - error connecting to {hostname}".format(hostname=args.hostname))
    sys.exit(3)

if r.ok:
    returnState = "OK"
    exitCode = 0
else:
    returnState = "CRITICAL"
    exitCode = 2

print("{returnState} - API response code from {hostname} is {code}| time={time}s".format(returnState=returnState,hostname=args.hostname,code=r.status_code,time=r.elapsed.total_seconds()))
sys.exit(exitCode)
